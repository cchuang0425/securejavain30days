# 安全地寫 Java 的 「基本功」- Day 12

## 前言

我們已經談完了 CIA、AAA、STRIDE 以及 DREAD 等資訊安全的目標與威脅等主題。接下來的日子，我們開始討論如何設計與實作安全的系統。在這方面，我不是專家；我只是將這些時間裡的經驗與學到的知識，整理匯集給各位，期望可以幫上些什麼。

## 最小權限(Least Privilege)

在系統設計時，總會存取檔案系統或資料庫，或某些資源。然而，我們常常忽略了一件事，那就是工程師可以用 root 帳號做很多事，但是當系統佈署給使用者時，使用者或者系統的任何一個程式，都不適合用 root 或 sa 帳號。甚至在這個原則裡，使用者與程式的權限，應當被限縮，只給予有必要的、最小的權限。有的系統更嚴謹的，會設定其權限可使用的時間區間。

## 責任分離(Seperation of duties)

在你的系統裡面，會不會常常有 admin 帳號，它什麼功能都能執行，什麼事都能做呢？我認識一位資深的前輩，說過一句話，雖然不見得每個人都能實踐在他的系統裡，但很有道理。「root 其實不適合用來做任何事，root 只適合用來派權限」，這句話點出了這個原則的精神。最大的權限，常常不應該是公司的 CEO 或者總經理所擁有，而是給予真正能為這個系統負責任、妥善管理的專職人員。並且，每個使用者能做什麼、不能做什麼，與其工作業務必須緊密相關連，而不能把所有大權全部授予某個使用者。

## 縱深防禦(Defense-in-Depth)

我們在設計系統時，常常會透過分層架構的方式進行抽象化的建構。但每一層，都可能有每一層的脆弱點。因此，每一層設計時，都需要考量安全風險並加以防禦，雖然單層無法完全抵禦所有攻擊，但所有分層的安全設計加總起來，卻可以大幅強化系統的安全防禦能力。

## 安全失敗(Fail safe)

我記得有一次，把 iPad 與藍芽鍵盤放在背包裡，結果藍芽鍵盤誤觸太多次，導致 iPad 的登入失敗超過十次。又剛好那時候的安全設定是，當登入超過十次時，系統自動清除。於是，我的 iPad 就完完全全地回到出廠預設狀態了。當系統失敗時，你的系統能夠維持好 CIA 嗎？安全失敗原則，就是把失敗考量進去，在系統發生失效、錯誤、例外、崩潰等狀況時，可以在最短的時間之內保護資料與服務的 CIA 品質。

## 簡單機制(Economy of mechanism)

聽說，雞蛋的圓弧型結構，是能在最精簡的材料與設計當中，取得最大載重能力的結構。因此，世界知名的音樂廳，常常是圓頂型的設計，不是因為有一隻大母雞要孵蛋，而是為要使用這樣圓弧型的結構做到最佳的聲學效果。我們在設計系統時候，也是如此。其實，越是複雜的東西，常常越不堪一擊。當你想要把系統做安全時，其實反而要往單純簡潔的架構方面著想。這個原則本身上不會與縱深防禦衝突，因為分層過程中，可以層層防護。但設計時，必須言之有物而意賅。有個名詞很接近，叫 KISS 原則，中文翻「持簡守愚」意即在此。

## 小結

其實如果有人上過 CISSP 或 CSSLP，應該不陌生這些內容。今年剛好有機會可以上到 CSSLP 的內容，這些對平常在開發與設計上面，其實發揮了相當大的幫助。接下來尚有另外五項原則，留待明日的內容，我會繼續介紹完。