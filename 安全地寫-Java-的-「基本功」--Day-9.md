# 安全地寫 Java 的 「基本功」- Day 9

## 前言

昨天簡單地說明了風險的量化估計方式，你一定覺得好像缺了什麼。風險？怎麼算是一回事。我們好像沒有說到，究竟是哪些風險，會造成系統的資訊安全問題。

今天同樣要借用 Microsoft 的另一套模型，來解釋這一件事。

## STRIDE

假設，我們想寫一個不需要與其他系統介接，不需要跟使用者互動，不需要跑在任何作業系統的軟體。只有一個可能，就是你的教授要你把程式寫在答案紙上，而不是寫在 IDE 裡面。

軟體就是要與其他的外在因子相互接連，才能成就它的意義。然而，也就是因為與外界相連，便帶來了相當程度的風險。在風險分析方法當中，我個人認為最重要的，是這一套 STRIDE 模型。它分別代表：
    * **S**poofing identity：身份假冒
    * **T**empering with data：竄改資料
    * **R**epudiation：否認
    * **I**nformation disclosure：資訊洩漏
    * **D**enial of service：拒絕服務
    * **E**levation of privilege：提高權限

為什麼呢，還記得我們最先介紹的 CIA 與 AAA 嗎？其實 STRIDE 就是由 CIA/AAA 所衍生而來的攻擊手法：
    * **S**poofing identity：**A**uthentication
    * **T**empering with data：**I**ntegrity
    * **R**epudiation：**A**udit/**A**ccountability
    * **I**nformation disclosure：**C**onfidentiality
    * **D**enial of service：**A**vailability
    * **E**levation of privilege：**A**uthorization

有人可能會覺得，不是要講資訊安全風險嗎？那我們應該講 SQL/Command Injection 或 XSS、CSRF 等攻擊。為什麼需要講這種理論呢？

我想，即使今天有 SQL/Command Injection 的發生，甚至導致主機已經可以 RCE(Remote code execution)，但使用者若只是極極極極微小的權限，那今天可憐的攻擊者大概只能 ls 一下你有哪些檔案，是不是 log 檔太肥，順手幫你清一下。

攻擊的手法是工具，但 STRIDE 是攻擊的動機與根源。

## 很抱歉的小結

因為今天實在忙碌，所以要跟各位閱讀本文的讀者說聲抱歉。原訂的篇幅，我必須分天進行。明日希望能深入地說明 STRIDE 每一項的含意。